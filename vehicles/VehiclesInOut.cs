﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vehicles
{
    class VehiclesInOut
    {
        public string vs { get; set;}
        public int vehicleID { get; set; }
        public long subscriptionID { get; set; }
        public int driverID { get; set; }
        public DateTime timeIn { get; set; }
        public float longitudeIn { get; set; }
        public float latitudeIn { get; set; }
        public double distance { get; set; }
        public DateTime timeOut { get; set; }
        public float longitudeOut { get; set; }
        public float latitudeOut { get; set; }
        public double timeSpent { get; set; }
    }
}
