//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace vehicles
{
    using System;
    
    public partial class CarsInRadiusByTime_Result
    {
        public string vs { get; set; }
        public short vehicleID { get; set; }
        public long SubscriptionID { get; set; }
        public short driverID { get; set; }
        public System.DateTime time { get; set; }
        public Nullable<float> latitude { get; set; }
        public Nullable<float> longitude { get; set; }
        public Nullable<double> DistanceInKM { get; set; }
    }
}
