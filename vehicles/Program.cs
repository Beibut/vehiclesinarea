﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vehicles
{
    class Program
    {
        public static MVDW_MartEntities1 _context;
        enum Intersect
        {
            NO,
            YES,
            COLLINEAR,
            OnTheEdge
        }

        public const float eps = 0.5f;
        public const float epsDist =0.0f;//-7.225608E-07

        public static void Main(string[] args)
        {
            //FindInOutVehiclesInCircle(args[0], args[1], args[2], args[3], args[4]);
            List<GpsPoint> polygon = new List<GpsPoint>();
            //GpsPoint point1 = new GpsPoint(0,0); polygon.Add(point1);
            //GpsPoint point2 = new GpsPoint(0,5); polygon.Add(point2);
            //GpsPoint point3 = new GpsPoint(5,5); polygon.Add(point3);
            //GpsPoint point4 = new GpsPoint(5,0); polygon.Add(point4);

            //Kedendyk 3.9 polygon coordinates
            GpsPoint point1 = new GpsPoint(53.514278f, 46.095011f); polygon.Add(point1);
            GpsPoint point2 = new GpsPoint(53.513995f, 46.088644f); polygon.Add(point2);
            GpsPoint point3 = new GpsPoint(53.516961f, 46.088316f); polygon.Add(point3);
            GpsPoint point4 = new GpsPoint(53.516947f, 46.086182f); polygon.Add(point4);
            GpsPoint point5 = new GpsPoint(53.51813f, 46.085696f); polygon.Add(point5);
            GpsPoint point6 = new GpsPoint(53.518188f, 46.083983f); polygon.Add(point6);
            GpsPoint point7 = new GpsPoint(53.514899f, 46.081811f); polygon.Add(point7);
            GpsPoint point8 = new GpsPoint(53.516192f, 46.079862f); polygon.Add(point8);
            GpsPoint point9 = new GpsPoint(53.514716f, 46.079541f); polygon.Add(point9);
            GpsPoint point10 = new GpsPoint(53.514129f, 46.077364f); polygon.Add(point10);
            GpsPoint point11 = new GpsPoint(53.514962f, 46.076892f); polygon.Add(point11);
            GpsPoint point12 = new GpsPoint(53.513277f, 46.074722f); polygon.Add(point12);
            GpsPoint point13 = new GpsPoint(53.511094f, 46.075025f); polygon.Add(point13);
            GpsPoint point14 = new GpsPoint(53.507732f, 46.076865f); polygon.Add(point14);
            GpsPoint point15 = new GpsPoint(53.508258f, 46.087939f); polygon.Add(point15);
            GpsPoint point16 = new GpsPoint(53.506016f, 46.087988f); polygon.Add(point16);
            GpsPoint point17 = new GpsPoint(53.510858f, 46.092059f); polygon.Add(point17);
            GpsPoint point18 = new GpsPoint(53.513318f, 46.09098f); polygon.Add(point18);
            GpsPoint point19 = new GpsPoint(53.51397f, 46.09193f); polygon.Add(point19);
            GpsPoint point20 = new GpsPoint(53.511962f, 46.092979f); polygon.Add(point20);


            for (int i = 0; i < args.Length; i+=2)
            {
                bool test = isPointInPolygon(args[i], args[i+1], polygon);
                Console.WriteLine(test ? "The point is INSIDE the polygon" : "The point is OUTSIDE the polygon");
            }
            Console.ReadKey();
        }


        public static bool isPointInPolygon(string xStr, string yStr, List<GpsPoint> Polygon)
        {
            float X = float.Parse(xStr, CultureInfo.InvariantCulture.NumberFormat);
            float Y = float.Parse(yStr, CultureInfo.InvariantCulture.NumberFormat);
            GpsPoint point = new GpsPoint(X, Y);
            float xMax = Polygon.Max(p => p.X);
            float xMin = Polygon.Min(p => p.X);
            float yMax = Polygon.Max(p => p.Y);
            float yMin = Polygon.Min(p => p.Y);

            //if point is outside min/max boundaries of polygon
            if (point.X < xMin || point.X > xMax || point.Y < yMin || point.Y > yMax)
            {
                return false;
            }

            //TODO: try different epsilon
            //float eps = (xMax - xMin) / 100;
            GpsPoint rayEnd = new GpsPoint(point.X-eps, yMax + eps);
            // Test the ray against all sides
            int intersections = 0;
            for (int v = 1; v < Polygon.Count(); v++)
            {
                Intersect res = intersects(point, rayEnd, Polygon[v - 1], Polygon[v], eps);
                if (res == Intersect.OnTheEdge) return true;
                if (res == Intersect.YES)
                    intersections++;
                else if (res == Intersect.COLLINEAR)
                    return true;
            }
            Intersect last = intersects(point, rayEnd, Polygon.Last(), Polygon.First(), eps);
            if (last== Intersect.YES)
                intersections++;
            else if (last == Intersect.COLLINEAR)
                return true;


            //Result
            if ((intersections & 1) == 1)
            {
                // Inside of polygon
                return true;
            }
            else
            {
                // Outside of polygon
                return false;
            }
        }

        private static Intersect intersects(GpsPoint point, GpsPoint rayEnd, GpsPoint polygonV1, GpsPoint polygonV2, float eps)
        {
            float a1, b1, c1;
            //create ray formula
            a1 = rayEnd.Y - point.Y;
            b1 = point.X - rayEnd.X;
            c1 = (rayEnd.X * point.Y) - (point.X * rayEnd.Y);

            float d1, d2, a2, b2, c2, d3,d4;
            // Every point (x,y), that solves the equation above, is on the line,
            // every point that does not solve it, is not. The equation will have a
            // positive result if it is on one side of the line and a negative one 
            // if is on the other side of it. We insert (x1,y1) and (x2,y2) of vector
            // 2 into the equation above.
            d1 = (a1 * polygonV1.X) + (b1 * polygonV1.Y) + c1;
            d2 = (a1 * polygonV2.X) + (b1 * polygonV2.Y) + c1;

            // If d1 and d2 both have the same sign, they are both on the same side
            // of our line 1 and in that case no intersection is possible. Careful, 
            // 0 is a special case, that's why we don't test ">=" and "<=", 
            // but "<" and ">".
            if (d1 > 0 && d2 > 0) return Intersect.NO;
            if (d1 < 0 && d2 < 0) return Intersect.NO;
            

            //create side = Polygon[v-1] and Polygon[v]
            a2 = polygonV2.Y - polygonV1.Y;
            b2 = polygonV1.X - polygonV2.X;
            c2 = (polygonV2.X * polygonV1.Y) - (polygonV1.X * polygonV2.Y);

            // Calculate d1 and d2 again, this time using points of vector 1.
            d3 = (a2 * point.X) + (b2 * point.Y) + c2;
            d4 = (a2 * rayEnd.X) + (b2 * rayEnd.Y) + c2;

            // Test if current side intersects with ray.
            // Again, if both have the same sign (and neither one is 0),
            // no intersection is possible.
            if (d3 > 0 && d4 > 0) return Intersect.NO;
            if (d3 < 0 && d4 < 0) return Intersect.NO;

            // If we get here, only two possibilities are left. Either the two
            // vectors intersect in exactly one point or they are collinear, which
            // means they intersect in any number of points from zero to infinite.
            if ((a1 * b2) - (a2 * b1) == 0.0f) return Intersect.COLLINEAR;

            if (Math.Abs(d3) <= epsDist && Math.Abs(d4)> epsDist && d1*d2 <0) return Intersect.OnTheEdge;

            if (Math.Abs(d3) > epsDist && Math.Abs(d4) <=epsDist) return Intersect.NO;

            if (Math.Abs(d3) <= epsDist && Math.Abs(d4) > epsDist && Math.Abs(d1) <=epsDist) return Intersect.OnTheEdge;

            if (Math.Abs(d3) <= epsDist && Math.Abs(d4)> epsDist && Math.Abs(d2) <= epsDist ) return Intersect.NO;

            return Intersect.YES;
        }

        //to find vehicles in and out to a circle with center at long,lat and radius rad at datefrom to dateto
        public static void FindInOutVehiclesInCircle(string longStr, string latStr, string radStr, string dateFromStr, string dateToStr)
        {
            _context = new MVDW_MartEntities1();
            float longitude = float.Parse(longStr, CultureInfo.InvariantCulture.NumberFormat);
            float lattitude = float.Parse(latStr, CultureInfo.InvariantCulture.NumberFormat);
            double radiusInKm = double.Parse(radStr, CultureInfo.InvariantCulture.NumberFormat);
            DateTime dateFrom = DateTime.ParseExact(dateFromStr, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            DateTime dateTo = DateTime.ParseExact(dateToStr, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            //request for radius +1 so that we can see when the car was out
            var fileInfo = new FileInfo("C:\\Users\\cyxf\\Desktop\\Analytics\\Motor Vehicle\\3GI new coords.xlsx");
            ExcelPackage excel = new ExcelPackage(fileInfo);

            for (var day = dateFrom.Date; day.Date <= dateTo.Date; day = day.AddDays(2))
            {
                var nextDay = day.AddDays(2);
                var vehiclesInRadius = _context.CarsInRadiusByTime(longitude, lattitude, radiusInKm + 1, day, nextDay).ToList();
                List<VehiclesInOut> result = new List<VehiclesInOut>();
                int N = vehiclesInRadius.Count();
                if (N <= 0)
                    continue;

                int i = 0;
                int resLastID = -1;

                while (i < N)
                {
                    if (i < N && vehiclesInRadius.ElementAt(i).DistanceInKM > radiusInKm)
                        while (i < N && vehiclesInRadius.ElementAt(i).DistanceInKM > radiusInKm)
                            i++;
                    if (i < N)
                    {
                        resLastID++;
                        VehiclesInOut first = new VehiclesInOut();
                        first.vs = vehiclesInRadius.ElementAt(i).vs;
                        first.subscriptionID = vehiclesInRadius.ElementAt(i).SubscriptionID;
                        first.vehicleID = vehiclesInRadius.ElementAt(i).vehicleID;
                        first.driverID = vehiclesInRadius.ElementAt(i).driverID;
                        first.timeIn = vehiclesInRadius.ElementAt(i).time;
                        first.longitudeIn = vehiclesInRadius.ElementAt(i).longitude ?? 0;
                        first.latitudeIn = vehiclesInRadius.ElementAt(i).latitude ?? 0;
                        first.distance = vehiclesInRadius.ElementAt(i).DistanceInKM ?? 0;
                        result.Add(first);
                        i++;
                    }

                    while (i < N && vehiclesInRadius.ElementAt(i).vehicleID == result[resLastID].vehicleID &&
                        (vehiclesInRadius.ElementAt(i).DistanceInKM <= radiusInKm
                        || (vehiclesInRadius.ElementAt(i).time - vehiclesInRadius.ElementAt(i - 1).time).Minutes < 10))
                        i++;

                    if (i < N)
                    {
                        result[resLastID].timeOut = vehiclesInRadius.ElementAt(i - 1).time;
                        result[resLastID].longitudeOut = vehiclesInRadius.ElementAt(i - 1).longitude ?? 0;
                        result[resLastID].latitudeOut = vehiclesInRadius.ElementAt(i - 1).latitude ?? 0;
                        result[resLastID].timeSpent = (result[resLastID].timeOut - result[resLastID].timeIn).TotalMinutes;
                    }
                }
                var workSheet = excel.Workbook.Worksheets.Add(day.ToString());

                for (int j = 0; j < result.Count; j++)
                {
                    workSheet.Cells[j + 2, 1].Value = result[j].vs;
                    workSheet.Cells[j + 2, 2].Value = result[j].vehicleID;
                    workSheet.Cells[j + 2, 3].Value = result[j].driverID;
                    workSheet.Cells[j + 2, 4].Value = result[j].timeIn;
                    workSheet.Cells[j + 2, 5].Value = result[j].longitudeIn;
                    workSheet.Cells[j + 2, 6].Value = result[j].latitudeIn;
                    workSheet.Cells[j + 2, 7].Value = result[j].distance;
                    workSheet.Cells[j + 2, 8].Value = result[j].longitudeOut;
                    workSheet.Cells[j + 2, 9].Value = result[j].latitudeOut;
                    workSheet.Cells[j + 2, 10].Value = result[j].timeOut;
                    workSheet.Cells[j + 2, 11].Value = result[j].timeSpent;
                }
                excel.Save();
            }
        }
    }
}
