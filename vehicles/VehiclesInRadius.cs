﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vehicles
{
    class VehiclesInRadius
    {
        public int? vehicleID { get; set; }
        public int? driverID { get; set; }
        public DateTime? time { get; set; }
        public float? longitude { get; set; }
        public float? distance { get; set; }
        public float? latitude { get; set; }
    }
}
